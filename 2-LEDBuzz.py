import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)

GPIO.setup(18, GPIO.OUT) #RED Light
GPIO.setup(24, GPIO.OUT) #Blue Light
GPIO.setup(22, GPIO.OUT) #buzzer

print ("lights on and sound on")

GPIO.output(18, GPIO.HIGH)
GPIO.output(24, GPIO.HIGH)
GPIO.output(22, GPIO.HIGH)

# Pause for 1 sec
time.sleep(0.5)

GPIO.output(18, GPIO.LOW)
GPIO.output(24, GPIO.LOW)
GPIO.output(22, GPIO.LOW)

GPIO.cleanup()
